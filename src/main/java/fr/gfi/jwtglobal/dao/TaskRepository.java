package fr.gfi.jwtglobal.dao;

import fr.gfi.jwtglobal.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TaskRepository extends JpaRepository<Task, Long> {
}
