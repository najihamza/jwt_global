package fr.gfi.jwtglobal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * montrer à spring comment il va trouver les roles et user
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("{noop}admin")
//                .roles("ADMIN","USER")
//        .and()
//        .withUser("user")
//        .password("{noop}1234")
//        .roles("USER");
        auth.userDetailsService(userDetailsService)
        .passwordEncoder(bCryptPasswordEncoder);// verification du password en considerant qu'il est en Bcrypt




    }


    /**
     * les droits d'access, quelle routes, ...
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.httpBasic().disable();
        http.csrf().disable();
        http.formLogin();
        http
                .authorizeRequests()
                .antMatchers("/login/**","/register/**")
                .permitAll();
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "tasks/**")
                .hasAuthority("ADMIN");
        http.authorizeRequests().anyRequest().authenticated();
    }


}
