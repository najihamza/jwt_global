package fr.gfi.jwtglobal.service;

import fr.gfi.jwtglobal.dao.AppRoleRepository;
import fr.gfi.jwtglobal.dao.AppUserRepository;
import fr.gfi.jwtglobal.entities.AppRole;
import fr.gfi.jwtglobal.entities.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;//crypter le mot de passe avant de creer le user dans la BD

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private AppRoleRepository appRoleRepository;

    @Override
    public AppUser saveUser(AppUser appUser) {
        String hashPW = bCryptPasswordEncoder.encode(appUser.getPassword());
        appUser.setPassword(hashPW);
        return appUserRepository.save(appUser);
    }

    @Override
    public AppRole saveRole(AppRole appRole) {
        return appRoleRepository.save(appRole);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        AppRole appRole = appRoleRepository.findByRoleName(roleName);
        AppUser appUser = appUserRepository.findByUsername(username);
        appUser.getRoles().add(appRole);
    }

    @Override
    public AppUser findUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }
}
