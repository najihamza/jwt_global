package fr.gfi.jwtglobal.service;

import fr.gfi.jwtglobal.entities.AppRole;
import fr.gfi.jwtglobal.entities.AppUser;

public interface AccountService {

    public AppUser saveUser(AppUser appUser);

    public AppRole saveRole(AppRole appRole);

    public  void addRoleToUser(String username, String roleName);

    public AppUser findUserByUsername(String username);

}
